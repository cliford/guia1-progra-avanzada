#!/usr/bin/env python3
# -*- coding:utf-8 -*-
from Comensal import Comensal


def a_pagar(santisimo, amigos, parte, total):
    """
        Se evalua si con el dinero entre todos se puede cancelar la cuenta
        Cabe destacar que Pepe no gasta más de la parte porque es su santo
        entonces no paga, es su regalo
    """
    se_puede, total = santisimo.dividir_cuenta(amigos, parte, total)
    if se_puede is True:
        print("{0} paga {1} USD".format(santisimo.nombre, santisimo.paga))
        for i in range(len(amigos)):
            print("{0} paga {1} USD".format(amigos[i].nombre, amigos[i].paga))
    else:
        pueden_pagar = []
        for i in range(len(amigos)):
            if amigos[i].get_disponible() > 0:
                pueden_pagar.append(amigos[i])
            else:
                print("{0} paga {1} USD".format(amigos[i].nombre,
                                                amigos[i].paga))
        if len(pueden_pagar) > 0:
            a_pagar(santisimo, pueden_pagar, total, len(pueden_pagar))
        else:
            print("No se cubre la cuota.")


if __name__ == "__main__":
    try:
        # persona de santo
        santisimo = Comensal("Pepe", "Pepón", 50)
        amigos = []
        print("¡Hola, Pepe!\nFeliz Santo.")
        total = float(input("Ingresa el total de la cuenta (en USD): "))
        # cantidad de amigos
        rial = 2
        if total > 0:
            print("Ahora ingrese los datos de quienes celebraron con usted.")
            # se crean todos los amigos
            for i in range(rial):
                print('Amigo {0}'.format(i + 1))
                nombre = str(input('Nombre:'))
                apellido = str(input('Apellido: '))
                disponible = int(input('Monto disponible para pagar: '))
                if disponible > 0:
                    amigos.append(Comensal(nombre, apellido, disponible))
                else:
                    print("No existe esa cantidad de dinero.")
                    break

            # lo que tiene pagar cada persona
            parte = total / (rial + 1)
            santisimo.loquepaga_santo(parte, total)
            a_pagar(santisimo, amigos, parte, total)
        else:
            print("No existe esa cantidad de dinero.")

    except ValueError:
        print("Ingresaste los datos mal.")
