#!/usr/bin/env python3
# -*- coding:utf-8 -*-


class Cuenta():

    def __init__(self, nombre, numero, saldo):
        self.nombre = nombre
        self.__numero = numero
        self.__saldo = saldo

    def get_numero(self):
        return self.__numero

    def get_saldo(self):
        return self.__saldo

    def set_saldo(self, monto):
        self.__saldo = self.get_saldo() + monto

    def giro_saldo(self, monto):
        # no se puede girar más de lo que se tiene
        if self.get_saldo() >= monto > 0:
            # al girar, se resta dinero
            self.set_saldo(monto * -1)
            return True
        else:
            return False

    def ingreso_saldo(self, monto):
        # no se puede agregar 0 o menos
        if monto > 0:
            self.set_saldo(monto)
            return True
        else:
            return False

    def transferencia(self, monto, cuentaTransferencia):
        # no se puede transferir más de lo que se tiene
        if self.get_saldo() >= monto > 0:
            cuentaTransferencia.set_saldo(monto)
            # al transferir, se resta dinero
            self.set_saldo(monto * -1)
            return True
        else:
            return False
