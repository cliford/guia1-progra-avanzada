#!/usr/bin/env python3
# -*- coding:utf-8 -*-
from Cuenta import Cuenta


def menu(cuentaOriginal):
    try:
        print("Ingrese opción:\n1. Ver datos de cuenta.\n2. Giro de dinero.")
        print("3. Ingreso de dinero.\n4. Transferencia a otra cuenta")
        opcion = int(input())
        if opcion == 1:
            print("Nombre: {0}".format(cuentaOriginal.nombre))
            print("Número: {1}".format(cuentaOriginal.get_numero()))
            print("Saldo (USD): {0}".format(cuentaOriginal.get_saldo()))
        # cualquier opcion restante requiere de un monto
        elif opcion == 2 or opcion == 3 or opcion == 4:
            monto = float(input('Monto de ingreso: '))
            if opcion == 2:
                accion = cuentaOriginal.giro_saldo(monto)
            elif opcion == 3:
                accion = cuentaOriginal.ingreso_saldo(monto)
            else:
                # datos de cuenta de transferencia
                print("Ingrese datos de la cuenta: ")
                cuentaTransfer = (Cuenta(str(input("Nombre: ")),
                                  float(input("Número: ")), 0))
                accion = cuentaOriginal.transferencia(monto, cuentaTransfer)
            # accion determina si la acción se pudo realizar
            if accion is True:
                print("La transacción se hizo con éxito.")
            else:
                print("Lo siento, hubo un problema.")
                print("Para la próxima ingrese con cuidado los datos.")
        else:
            print("Opción no válida.")
    except ValueError:
        print("Opción no válida.")


if __name__ == "__main__":
    cuentaOriginal = Cuenta("Jem Carstairs", 982033847622.0, 1000.0)
    menu(cuentaOriginal)
