#! /usr/bin/env python3
# -*- coding:utf-8 -*-


class Numero:

    def __init__(self, numero):
        self.numero = numero
        self.final_n1 = []
        self.final_n2 = []

    def reemplazar(self):

        # for que recorre la lista
        for i in range(len(self.numero)):
            # cunado indice es 0 se iguala a la primera posicion(no cambia)
            if i == 0:
                self.final_n1.append(self.numero[i])
            # agrupar el valor anteriormente alculado y sumarle el ultimo
            # digito de la lista
            # si la suma supera los 2 digitos se deja el ultimp
            else:
                if self.numero[i] + self.final_n1[i-1] > 9:
                    x = (self.numero[i] + self.final_n1[i-1]) % 10
                    self.final_n1.append(x)
                else:
                    self.final_n1.append(self.numero[i] + self.final_n1[i-1])

    def getRemplazar(self):
        return self.final_n1

    # sumar a todos los digitos 7
    def sumar_7(self):
        for i in range(len(self.numero)):
            self.final_n2.append(7 + (self.numero[i]))

    def getSumar_7(self):
        return self.final_n2

    # intercambiar solo los indices 1 --> 3 y 2 --> 4 del resultado de sumar
    def intercambio(self):
        temp = self.final_n2[2]
        self.final_n2[2] = self.final_n2[0]
        self.final_n2[0] = temp

        temp = self.final_n2[3]
        self.final_n2[3] = self.final_n2[1]
        self.final_n2[1] = temp

    def getIntercambio(self):
        return self.final_n2
