#!/usr/bin/env python3
# -*- coding:utf-8 -*-
from Perro import Perro
import random


def acciones_perros(accion, perros):
    # en listas los valores negativos si funcionan
    perros[accion - 2].comer()
    perros[accion].jugar(perros[accion - 1])
    perros[accion - 1].ladrar()
    perros[accion].pelear(perros[accion - 1])


if __name__ == "__main__":
    perros = []
    perros.append(Perro("Conan", "Café", "Golden", 1, "macho", 20))
    perros.append(Perro("Nairobi", "Negro", "Dobermann", 2, "hembra", 25))
    perros.append(Perro("Shapu", "Blanco", "Poodle", 12, "macho", 6))

    print('Tenemos 3 perros: ')
    for i in range(3):
        print("{0}. {1}: Color {2}, {3}, {4} años, {5}, {6} kg.".format(
                i + 1, perros[i].nombre, perros[i].color, perros[i].raza,
                perros[i].edad, perros[i].sexo, perros[i].peso))

    choice = str(input("¿Quiere saber que están haciendo? (S para sí): "))
    if choice.upper() == 'S':
        accion = random.randrange(3)
        acciones_perros(accion, perros)
    else:
        print("Adiós!")
