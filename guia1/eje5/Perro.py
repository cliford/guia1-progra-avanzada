#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import time
import random


class Perro():

    def __init__(self, nombre, color, raza, edad, sexo, peso):
        self.nombre = nombre
        self.color = color
        self.raza = raza
        self.edad = edad
        self.sexo = sexo
        self.peso = peso

    # acciones
    def comer(self):
        time.sleep(1)
        print('{0} está comiendo.'.format(self.nombre))
        # cuanto demora en comer
        sucesion = True
        while sucesion is True:
            time.sleep(1)
            print("...")
            # para más variedad en la duración del ciclo 3 True y 1 False
            sucesion = random.choice([True, False, True, True])
        time.sleep(1)
        print('{0} quedó satisfecha/o.'.format(self.nombre))

    def jugar(self, perrito):
        time.sleep(1)
        print("{0} está jugando con {1}.".format(self.nombre, perrito.nombre))

    def ladrar(self):
        time.sleep(1)
        print("{0} está ladrando.".format(self.nombre))

    def pelear(self, perrito):
        time.sleep(1)
        # si son del mismo sexo, pelean
        if self.sexo == perrito.sexo:
            print("¡Se han puesto a pelear!")
            # para que peleen un rato
            for i in range(3):
                time.sleep(1)
                print("...")
            win = random.choice([self.nombre, perrito.nombre])
            print("¡Ha ganado {0}!".format(win))
        else:
            print("{0} se aleja.".format(self.nombre))
