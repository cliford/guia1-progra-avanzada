#! /usr/bin/env python3
# -*- coding:utf-8 -*-

from Tarjeta import Tarjeta


def compra(nombre, dinero, titular):
    while True:
        print("1.-comprar")
        print("2.ver saldo")
        print("3.-salir")
        opcion = int(input("que desea hacer?: "))
        if titular.getDinero() == 0:
            print("No tiene mas dinero que gastar")
            break
        else:
            if opcion == 1:
                # compra
                titular.genCompra()

            elif opcion == 2:
                # saldo
                print("Su saldo es: {0}".format(titular.getDinero()))
            elif opcion == 3:
                # salir
                #
                print("Chaochao")
                break
            else:
                # error
                print("Error ejecute el programa de nuevo")
                break


if __name__ == "__main__":

    nombre = str(input("nombre de usuario:"))
    dinero = int(input("ingrese monto:  "))

    titular = Tarjeta(nombre, dinero)

    print("Titular de la tarjeta es:  {0} ".format(titular.nombre))
    print("Monto en su cuenta bancaria es: {0}".format(titular.dinero))

    compra(titular.nombre, titular.dinero, titular)

