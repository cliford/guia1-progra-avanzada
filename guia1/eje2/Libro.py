#! /usr/bin/env python3
# -*- coding:utf-8 -*-
from Biblioteca import Biblioteca


class Libro:
 
    def __init__(self, biblioteca):
        self.biblioteca = biblioteca

    def asociar_biblioteca(self, biblioteca):
        if isinstance(biblioteca, Biblioteca):
            self.biblioteca = biblioteca

    def agregarLibro(self):
        # agregar valores a la biblioteca
        self.libro = str(input("Ingrese libro: "))
        self.autor = str(input("Ingrese autor del libro:"))
        self.total = int(input("Ingrese Total de libros:"))
        self.prestamo = int(input("Ingrese libros prestados:"))
        print("\n")
        self.biblioteca.append([self.libro, self.autor,
                                self.total, self.prestamo])

    # retornar biblioteca con datos
    def getBiliboteca(self):
        return self.biblioteca




